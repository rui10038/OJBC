/*
 * Created by JFormDesigner on Thu Apr 25 10:04:58 CST 2019
 */

package com.wuye.view;

import com.jgoodies.forms.factories.*;
import com.wuye.bean.DateBaseSave;
import com.wuye.dao.Core;
import com.wuye.dao.Mysql.GoIntoDatabase;
import com.wuye.dao.Mysql.GoIntoTable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author 8
 */
public class Table extends JPanel {
    public Table() {
        initComponents();
    }
    public static void  test(Object [][] objects,String []strings){
       Table table=new Table();
        JFrame frame =table.frame1;
        try {
            //Nimbus风格
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());//当前系统风格
//UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");//Motif风格，是蓝黑
//UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());//跨平台的Java风格
//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");//windows风格
//UIManager.setLookAndFeel("javax.swing.plaf.windows.WindowsLookAndFeel");//windows风格
//UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");//java风格
//UIManager.setLookAndFeel("com.apple.mrj.swing.MacLookAndFeel");//待考察，
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        frame.setContentPane(table.scrollPane1);
        JTable table1 = table.table1;
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        System.out.println("自定义");
        table1.setModel(new DefaultTableModel(
                objects,
                strings
        ){ @Override
        public boolean isCellEditable(int row, int column)
        {
            return false;
        }});

    //获得Toolkit对象
        Toolkit tk= frame.getToolkit();
        int width=500;
        int height=225;
        //得到当前屏幕的尺寸，即高度和宽度
        Dimension dimension=tk.getScreenSize();
        //设置窗口位置居中，屏幕尺寸减去窗口大小处于2
        frame.setLocation((int) (dimension.getWidth()-width)/2,(int)(dimension.getHeight()-height)/2);
        frame.pack();
        frame.setVisible(true);
    }

    private void table1MouseClicked(MouseEvent e) {
        if(e.getClickCount() == 2) {
            //获得行位置
            int row = ((JTable) e.getSource()).rowAtPoint(e.getPoint());
            //获得列位置
            int col = ((JTable) e.getSource()).columnAtPoint(e.getPoint());
            //获得点击单元格数据
            String cellVal=(String)(table1.getValueAt(row,col));
            System.out.println(row+"--------"+col+"-------"+cellVal+"allTable");
            Core.dateBaseSave.setTableName(cellVal);
            GoIntoTable.intoTable(Core.dateBaseSave);
        };
    }
    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        frame1 = new JFrame();
        menuBar1 = new JMenuBar();
        menu1 = new JMenu();
        menuItem1 = new JMenuItem();
        menuItem2 = new JMenuItem();
        menuItem3 = new JMenuItem();
        menu4 = new JMenu();
        menu2 = new JMenu();
        menu5 = new JMenu();
        menu3 = new JMenu();
        scrollPane1 = new JScrollPane();
        table1 = new JTable();
        title1 = compFactory.createTitle("\u6240\u6709\u8868");

        //======== frame1 ========
        {
            Container frame1ContentPane = frame1.getContentPane();
            frame1ContentPane.setLayout(null);

            //======== menuBar1 ========
            {

                //======== menu1 ========
                {
                    menu1.setText("\u65b0\u589e");

                    //---- menuItem1 ----
                    menuItem1.setText("\u65b0\u589e\u8868");
                    menu1.add(menuItem1);

                    //---- menuItem2 ----
                    menuItem2.setText("\u65b0\u589e\u5b57\u6bb5");
                    menu1.add(menuItem2);

                    //---- menuItem3 ----
                    menuItem3.setText("\u63d2\u5165\u6570\u636e");
                    menu1.add(menuItem3);
                }
                menuBar1.add(menu1);

                //======== menu4 ========
                {
                    menu4.setText("           ");
                }
                menuBar1.add(menu4);

                //======== menu2 ========
                {
                    menu2.setText("\u7f16\u8f91");
                }
                menuBar1.add(menu2);

                //======== menu5 ========
                {
                    menu5.setText("           ");
                }
                menuBar1.add(menu5);

                //======== menu3 ========
                {
                    menu3.setText("\u5220\u9664");
                }
                menuBar1.add(menu3);
            }
            frame1.setJMenuBar(menuBar1);

            //======== scrollPane1 ========
            {

                //---- table1 ----
                table1.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        table1MouseClicked(e);
                    }
                });
                scrollPane1.setViewportView(table1);
            }
            frame1ContentPane.add(scrollPane1);
            scrollPane1.setBounds(0, 0, 530, 379);
            frame1ContentPane.add(title1);
            title1.setBounds(new Rectangle(new Point(35, -20), title1.getPreferredSize()));

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < frame1ContentPane.getComponentCount(); i++) {
                    Rectangle bounds = frame1ContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = frame1ContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                frame1ContentPane.setMinimumSize(preferredSize);
                frame1ContentPane.setPreferredSize(preferredSize);
            }
            frame1.pack();
            frame1.setLocationRelativeTo(frame1.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JFrame frame1;
    private JMenuBar menuBar1;
    private JMenu menu1;
    private JMenuItem menuItem1;
    private JMenuItem menuItem2;
    private JMenuItem menuItem3;
    private JMenu menu4;
    private JMenu menu2;
    private JMenu menu5;
    private JMenu menu3;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JLabel title1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
