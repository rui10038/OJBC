/*
 * Created by JFormDesigner on Sat Apr 20 21:09:30 CST 2019
 */

package com.wuye.view;

import com.jgoodies.forms.factories.*;
import com.wuye.bean.DateBaseSave;
import com.wuye.dao.Core;

import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author 8
 */
public class Main extends JPanel {
    public Main() {
        initComponents();
    }
    public static void main(String[] args) {
        Main main=new Main();
        JFrame frame1 =main.frame1;
        frame1.setContentPane(main.panel1);
        frame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Toolkit tk= frame1.getToolkit();//获得Toolkit对象
        int width=500;
        int height=225;
        Dimension dimension=tk.getScreenSize();//得到当前屏幕的尺寸，即高度和宽度
        frame1.setLocation((int) (dimension.getWidth()-width)/2,(int)(dimension.getHeight()-height)/2);//设置窗口位置居中，屏幕尺寸减去窗口大小处于2
        frame1.pack();
        frame1.setVisible(true);
    }

    /**
     * 登录方法
     * @param e
     */

    private void LoginActionPerformed(ActionEvent e) {
        System.out.println("登录");
        String text = textField1.getText();
        String pass = textField2.getText();
        String database = (String) comboBox1.getSelectedItem();
        if (text != null && pass != null && database !=null){
            label2.setText("用户名或者密码数据库不能为空");
        }
        System.out.println(text+","+pass+","+database);
        DateBaseSave dateBaseSave = new DateBaseSave();
        dateBaseSave.setName(text);
        Save(dateBaseSave);
        Core core =new Core();
        String coon = core.getCoon(text, pass, database,true);
        if ("OK".equals(coon)){
            JOptionPane.showMessageDialog(null,"登陆成功");
            frame1.dispose();
        }else {
            label2.setText(coon);
        }
    }
    private void TestActionPerformed(ActionEvent e){
        System.out.println("测试");
        String text = textField1.getText();
        String pass = textField2.getText();
        String database = (String) comboBox1.getSelectedItem();
        if (text != null && pass != null && database !=null){
            label2.setText("用户名或者密码数据库不能为空");
        }
        System.out.println(text+","+pass+","+database);
        Core core =new Core();
        String coon = core.getCoon(text, pass, database,false);
        if ("OK".equals(coon)){
            label2.setText("连接成功");
        }else {
            label2.setText(coon);
        }
    }
    public static DateBaseSave Save(DateBaseSave dateBaseSave){
        return dateBaseSave;
    }
    private void Exit(){

    }
    private void TestActionPerformed(AncestorEvent e) {
        // TODO add your code here
    }

    private void LoginAncestorAdded(AncestorEvent e) {
        // TODO add your code here
    }

    private void initComponents() {
        System.out.println("455");
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        frame1 = new JFrame();
        title1 = compFactory.createTitle("\u767b\u5f55");
        panel1 = new JPanel();
        label1 = new JLabel();
        Test = new JButton();
        Login = new JButton();
        name = new JLabel();
        password = new JLabel();
        textField1 = new JTextField();
        textField2 = new JTextField();
        Datebase = new JLabel();
        comboBox1 = new JComboBox<>();
        label2 = new JLabel();
        separator1 = compFactory.createSeparator("");

        //======== frame1 ========
        {
            Container frame1ContentPane = frame1.getContentPane();
            frame1ContentPane.setLayout(null);
            frame1ContentPane.add(title1);
            title1.setBounds(30, -50, 120, 27);

            //======== panel1 ========
            {

                //---- label1 ----
                label1.setText("\u6b22\u8fce\u6765\u5230OJBK\u6570\u636e\u5e93\u8fde\u63a5\u7a0b\u5e8f");

                //---- Test ----
                Test.setText("\u6d4b\u8bd5");
                Test.addActionListener(e -> TestActionPerformed(e));

                //---- Login ----
                Login.setText("\u767b\u5f55");
                Login.addAncestorListener(new AncestorListener() {
                    @Override
                    public void ancestorAdded(AncestorEvent e) {
                        LoginAncestorAdded(e);
                    }
                    @Override
                    public void ancestorMoved(AncestorEvent e) {}
                    @Override
                    public void ancestorRemoved(AncestorEvent e) {}
                });
                Login.addActionListener(e -> LoginActionPerformed(e));

                //---- name ----
                name.setText("\u7528\u6237\u540d\uff1a");

                //---- password ----
                password.setText("\u5bc6\u7801\uff1a");

                //---- Datebase ----
                Datebase.setText("\u6570\u636e\u5e93\uff1a");

                //---- comboBox1 ----
                comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
                    "Mysql",
                    "Oracle"
                }));

                GroupLayout panel1Layout = new GroupLayout(panel1);
                panel1.setLayout(panel1Layout);
                panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGroup(panel1Layout.createParallelGroup()
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGap(56, 56, 56)
                                    .addGroup(panel1Layout.createParallelGroup()
                                        .addComponent(Datebase, GroupLayout.Alignment.TRAILING)
                                        .addComponent(password, GroupLayout.Alignment.TRAILING)
                                        .addComponent(name, GroupLayout.Alignment.TRAILING))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(panel1Layout.createParallelGroup()
                                        .addComponent(comboBox1, GroupLayout.Alignment.TRAILING)
                                        .addComponent(textField2)
                                        .addComponent(textField1)))
                                .addComponent(label2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(Test)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Login)))
                            .addContainerGap())
                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                            .addGap(0, 0, Short.MAX_VALUE)
                            .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 658, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addGap(213, 213, 213)
                            .addComponent(label1)
                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );
                panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                            .addGap(9, 9, 9)
                            .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(name)
                                .addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addComponent(password)
                                .addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(13, 13, 13)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addComponent(Datebase)
                                .addComponent(comboBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(Login)
                                .addComponent(Test))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                            .addComponent(label2))
                );
            }
            frame1ContentPane.add(panel1);
            panel1.setBounds(0, -15, 645, 255);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < frame1ContentPane.getComponentCount(); i++) {
                    Rectangle bounds = frame1ContentPane.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = frame1ContentPane.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                frame1ContentPane.setMinimumSize(preferredSize);
                frame1ContentPane.setPreferredSize(preferredSize);
            }
            frame1.pack();
            frame1.setLocationRelativeTo(frame1.getOwner());
        }
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JFrame frame1;
    private JLabel title1;
    private JPanel panel1;
    private JLabel label1;
    private JButton Test;
    private JButton Login;
    private JLabel name;
    private JLabel password;
    private JTextField textField1;
    private JTextField textField2;
    private JLabel Datebase;
    private JComboBox<String> comboBox1;
    private JLabel label2;
    private JComponent separator1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
