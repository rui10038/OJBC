package com.wuye.dao.Mysql;

import com.wuye.bean.DateBaseSave;
import com.wuye.dao.Core;
import com.wuye.view.DateBase;
import com.wuye.view.Table;

import java.sql.*;

/**
 * @author 24507
 * Mysql 进入选择的数据库
 */
public class GoIntoDatabase{
    public static void intoDatabase (DateBaseSave database) {
        Core core = new Core();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection= DriverManager.getConnection("jdbc:mysql:///"+database.getDataBase()+"?characterEncoding=utf8",database.getName(),database.getPass());
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select table_name from information_schema.tables where table_schema='"+database.getDataBase()+"' order by table_name limit 0,30;");
            //刷新coon
            database.setConnection(connection);
            resultSet.last();
            //获得ResultSet的总行数
            int rowCount = resultSet.getRow();
            resultSet.beforeFirst();
            Object [][]objects=new Object[rowCount][1];
            String []arr = new String[1];
            arr[0]="table_name";
            System.out.println(rowCount);
            int i=0;
            while (resultSet.next()) {
                System.out.println(resultSet.getString("table_name")+"添加第"+i+"条");
                objects[i][0] =resultSet.getString("table_name");
                i++;
            }
            System.out.println(objects.length+"-------"+arr.length);
            Table.test(objects,arr);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

    }
}