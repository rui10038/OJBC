package com.wuye.dao;

import com.wuye.bean.DateBaseSave;
import com.wuye.view.DateBase;

import java.sql.*;

/**
 * @author 24507
 * 用于数据操控
 */
public class Core {
    public static DateBaseSave dateBaseSave;
    public String getCoon(String name, String pass, String type, boolean why){
        //通过构造方法传值给其他类
       dateBaseSave = new DateBaseSave();
        dateBaseSave.setName(name);
        dateBaseSave.setPass(pass);
        dateBaseSave.setType(type);
        try {
            Connection connection;
            if ("Mysql".equals(type)){
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql:///?characterEncoding=utf8",name,pass);
            }else {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                connection = DriverManager.getConnection("jdbc:oracle:thin:@//localhost:1521/ORCL",name,pass);
            }
            if (connection!=null){ System.out.println("连接成功");if (why){ dateBaseSave.setConnection(connection);mysqlAllDatebase(dateBaseSave);}else {oracleAllTables(connection);};return "OK";}

        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            String massage= String.valueOf(ex).replaceAll("user", "用户名:"+name);
            massage= massage.replaceAll("Access denied for", "拒绝访问,可能是用户名或者密码错误");
            massage= massage.replaceAll("using password:NO", "使用密码:否");
            massage= massage.replaceAll("using password:NO", "使用密码:是,密码为"+pass);
            massage= massage.replaceAll("invalid username/password; logon denied", "登录被拒绝,用户名/密码无效;登录被拒绝");
            massage= massage.replaceAll("The Network Adapter could not establish the connection", "网络适配器无法建立连接，可能是您的oracle服务没有启动，或者账号密码输入错误");
            return massage;
        }
        return null;
    }
    public void mysqlAllDatebase(DateBaseSave dateBaseSave){
        System.out.println("你好Mysql");
        try {
            Statement stat =dateBaseSave.getConnection().createStatement();
            ResultSet rs = stat.executeQuery("show databases;");
            rs.last();
            int rowCount = rs.getRow(); //获得ResultSet的总行数
            rs.beforeFirst();
            Object [][]objects=new Object[rowCount][1];
            String []arr = new String[1];
            arr[0]="Database";
            System.out.println(rowCount);
            int i=0;
            while (rs.next()) {
                System.out.println(rs.getString("Database")+"添加第"+i+"条");
                objects[i][0] =rs.getString("Database");
                i++;
            }
            DateBase.test(objects,arr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    };
    public void oracleAllTables(Connection connection){
        System.out.println("你好Oracle");
        try {
            Statement  stat = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet  rs = stat.executeQuery("select table_name from user_tables");
            rs.last();
            int rowCount = rs.getRow();
            //获得ResultSet的总行数
            rs.beforeFirst();
            Object [][]objects=new Object[rowCount][1];
            String []arr = new String[1];
            arr[0]="table_name";
            System.out.println(rowCount);
            int i=0;
            while (rs.next()) {
                System.out.println(rs.getString("table_name")+"添加第"+i+"条");
                objects[i][0] =rs.getString("table_name");
                i++;
            }
            System.out.println(objects.length+"-------"+arr.length);
            DateBase.test(objects,arr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
